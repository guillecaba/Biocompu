import bisect
import textwrap
import math
import  time
##Funcion para crear el Indice
def crearIndice(t,k):
#Crea una lista que contiene los k-meros ordenados alfabeticamente y sus posiciones en t
	indice = []
	for i in range(len(t) - k + 1):
		indice.append((t[i:i+k], i))
	indice.sort() #se ordena alfabeticamente
	return indice
##Funcion para consultar el indice
def consultar(p, indice, k):
	kmero = p[:k]
	i = bisect.bisect_left(indice, (kmero, -1))
	hits = []
	tamanho = len(indice)
	while i < tamanho: 
		if indice[i][0] != kmero:
			break
		hits.append(indice[i][1])
		i += 1
	return hits
##Funcion para Hacer Alineamiento Exacto 
def AlineamientoExactoIndices(p,t,indice,k):
	ocurrencias=[]
	for i in consultar(p,indice,k):
		if p[k:] == t[i+k:i+len(p)]:
			ocurrencias.append(i)
	return ocurrencias
def print_matrix(M):
	print('\n'.join([''.join(['{:<6}'.format(round(j, 3)) for j in i]) 
      for i in M]))
	print()

##Funcion de Distancia de Edicion Programacion Dinamica
def editDistance(x, y):
    # Create distance matrix
    D = []
    for i in range(len(x)+1):
        D.append([0]*(len(y)+1))
    # Initialize first row and column of matrix
    for i in range(len(x)+1):
        D[i][0] = i
    for i in range(len(y)+1):
        D[0][i] = i
    # Fill in the rest of the matrix
    for i in range(1, len(x)+1):
        for j in range(1, len(y)+1):
            distHor = D[i][j-1] + 1
            distVer = D[i-1][j] + 1
            if x[i-1] == y[j-1]:
                distDiag = D[i-1][j-1]
            else:
                distDiag = D[i-1][j-1] + 1
            D[i][j] = min(distHor, distVer, distDiag)
    # Edit distance is the value in the bottom right corner of the matrix

    return D[-1][-1]

def mas_comun(lst):
    return max(set(lst), key=lst.count)

	



print('<--------Inicio del Programa-------->')
inicio_de_tiempo = time.time()
##Se abre el genoma de referencia
file=open('phix.fa')
temp = [line[:-1] for line in file]
texto=''
cortar='>gi|216019|gb|J02482.1|PX1CG Coliphage phi-X174, complete genome'
i=0
for line in temp:
	if  i != 0:
		texto=texto+line
		
	i=1
texto=texto
#print("<------Este es el Genoma de Referencia------>\n",texto)


#print(len(texto))

print("\n")
filereads=open('reads_phix_1.fastq')
print('<------Abrio el archivo del Genoma de Referencia------>')
temp = [line[:-1] for line in filereads]
reads=[]
i=1
nrolines = 1
for line in temp:
	if nrolines % 4 == 2:
		reads.append(line)
	nrolines = nrolines + 1


#print("<------Estos son los reads------>\n")
#print(reads)






Indice=crearIndice(texto,5)
numeroderead=0
resultados=[]
print("<-------Se procesa cada read y luego cada particion------->")
for read in reads:

	
	#print('\nRead='+str(numeroderead+1),read)
	longitudread=math.ceil((len(read)/3))
	particiones=textwrap.wrap(read, longitudread)
	#print(particiones)

	

	
	resultados.append([])
	# resultados[0].append(0)
	# resultados[0].append(8)
	# print('Resultado= ',resultados[0])

	
	numeroParticion=0
	#print('Inicio del For')
	
	for particion in particiones:
		coincidenciaParticion=AlineamientoExactoIndices(particion,texto,Indice,5)
		numeroParticion=numeroParticion+1
		#print('\nNumero de particion= ',numeroParticion)
		#print('Coincidencia de Particion en el Genoma de Referencia= ',coincidenciaParticion)
		
		
		posiciones=[0,17,34]
		for posiciondeparticion in coincidenciaParticion:	
			posicionRealenT=posiciondeparticion-posiciones[numeroParticion-1]
			#print('Posicion Real= ',posicionRealenT)
			lineaent=texto[posicionRealenT:posicionRealenT+len(read)]
			if editDistance(read,lineaent) <= 2:
				resultados[numeroderead].append(posicionRealenT)
	#Elimina elementos repetidos de resultados
	resultados[numeroderead] = list(set(resultados[numeroderead]))
	#print('<-----Resultados de '+str(numeroderead+1),resultados[numeroderead])
	numeroderead=numeroderead+1
	

	
listaSolapada=[]
for posicionSolapada in range(len(texto)):
	listaSolapada.append([])
	for numeroderead in range(len(reads)):
		for resultadodelread in resultados[numeroderead]:
			if resultadodelread <= posicionSolapada and resultadodelread >= posicionSolapada-49:
				listaSolapada[posicionSolapada].append(reads[numeroderead][posicionSolapada-resultadodelread])

print('Candidatos para cada posicion')
# for posicionSolapada in range(len(texto)):
# 	print('Posicion='+str(posicionSolapada),listaSolapada[posicionSolapada])




Alineado=[]
for posicionSolapada in range(len(texto)):
	Alineado.append(mas_comun(listaSolapada[posicionSolapada]))

TextoAlineado=''	
for posicionSolapada in range(len(texto)):
	#print('Letra Ganadora en la posicion= '+str(posicionSolapada),Alineado[posicionSolapada])
	TextoAlineado=TextoAlineado+Alineado[posicionSolapada]

#print(TextoAlineado)

error=0
for posicion in range(len(texto)):
	if texto[posicion] != Alineado[posicion]:
		error=error+1

print("Diferencias entre Genoma de Referencia y Alineado ",error)

ListaAlineado=textwrap.wrap(TextoAlineado,70)

archivoResultado=open("Secuenciado.txt","w")
for line in ListaAlineado:
	archivoResultado.write(line)
	archivoResultado.write("\n")

archivoResultado.close()
print('<-----Genero el archivo de salida----->')




# numeroderead=0
# for read in reads:
# 	print('<-----Resultados de '+str(numeroderead+1),resultados[numeroderead])
# 	numeroderead=numeroderead+1


tiempo_final = time.time() 
tiempo_transcurrido = tiempo_final - inicio_de_tiempo
print ("\nTomo %d segundos." % (tiempo_transcurrido))
			
			


	
	

	


